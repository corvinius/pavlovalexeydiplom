﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : Observer
{
    private Weight weight;

    public Weight Weight
    {
        get
        {
            return weight;
        }
        set
        {
            weight = value;
        }
    }

    public Sensor(Weight weight)
    {
        this.Weight = weight;
    }

    public override void OnNotify()
    {
       
    }

    public override Weight GetWeight()
    {
        return Weight;
    }

}
