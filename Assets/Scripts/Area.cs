﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class Area: MonoBehaviour
    {
        public TrafficLight light;

        public float speed;

        public void Awake()
        {
            light = new TrafficLight(Random.Range(0, 3));
            speed = Random.Range(80, 120);
        }


        public void Update()
        {
            light.Update();
        }

    }
}
