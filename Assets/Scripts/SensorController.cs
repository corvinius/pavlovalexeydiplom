﻿using Accord;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.Math;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Distributions.Multivariate;
using Accord.Statistics.Distributions.Sampling;
using Accord.Statistics.Filters;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UnityEngine;

public class SensorController 
{

    private List<Sensor> sensors = new List<Sensor>();

    public Train train;

    private DecisionTree tree;

    private Codification codebook;

    public SensorController(Train train)
    {
        this.train = train;

        InitTree();



    }
    

    public void InitTree()
    {
        DataTable data = new DataTable("Sensors");

        data.Columns.Add("Variant", "Visual", "Traffic", "Event");

        data.Rows.Add("1", "0", "0", "Normal");
        data.Rows.Add("2", "1", "0",  "Stop");
        data.Rows.Add("3", "1", "0,5", "Stop");
        data.Rows.Add("4", "1", "1",  "Stop");
        data.Rows.Add("5", "0", "1",  "Stop");
        data.Rows.Add("6", "0", "0,5",  "MiddleSpeed");

        codebook = new Codification(data);
        DataTable symbols = codebook.Apply(data);


        int[][] inputs = symbols.ToJagged<int>("Visual", "Traffic");

        int[] outputs = symbols.ToArray<int>("Event");

        var id3learning = new ID3Learning()
        {
            new DecisionVariable("Visual",2),
            new DecisionVariable("Traffic", 3),
        };


        tree = id3learning.Learn(inputs, outputs);

    }

    public void AddSensor(Sensor sensor)
    {
        sensors.Add(sensor);
    }
    public void RemoveSensor(Sensor sensor)
    {
        sensors.Remove(sensor);
    }
    public void CheckWeight()
    {
        string[,] queryData = new string[sensors.Count, 2];
        //    {
        //        { "Visual","0"},
        //        {"Traffic","1" },
        //        { "Dispetcher","0"},

        //    };

        for (int i = 0; i < sensors.Count; i++)
        {
            queryData[i, 0] = sensors[i].GetWeight().Type.ToString();
            queryData[i, 1] = sensors[i].GetWeight().Value.ToString();
            Debug.Log(sensors[i].GetWeight().Type.ToString() + "  " + sensors[i].GetWeight().Value.ToString());
        }
      
       
        int[] query = codebook.Transform(queryData);

        int predicted = tree.Decide(query);

        string answer = codebook.Revert("Event", predicted);

        train.Event(answer);
     
    }
    public IEnumerator StartCheckSensors()
    {
        sensors.Sort((x, y) => x.GetWeight().Type.CompareTo(y.GetWeight().Type));

        while (true)
        {
            yield return new WaitForSeconds(1);
            CheckWeight();
        }
    }
}
