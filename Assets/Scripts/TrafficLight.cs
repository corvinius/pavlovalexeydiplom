﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrafficLight : UnitySensor
{
    

    public TrafficLight(float value)
    {
        if(value==0)
        {
            tempValue = 0;
        }
        else if(value==1)
        {
            tempValue = 0.5f;
        }
        else
        {
            tempValue = 1;
        }

    //    tempValue = value;
        Sensor = new Sensor(new Weight(TypeWeight.Traffic,tempValue));
    }

   
    
}
