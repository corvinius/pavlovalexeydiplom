﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitySensor 
{
    public Sensor Sensor;

    public float tempValue;

    public float Value
    {
        get
        {
            return Sensor.GetWeight().Value;
        }
        set
        {
            Sensor.GetWeight().Value = value;
        }
    }

    public void Update()
    {
        if(Value!=tempValue)
        {
            Value = tempValue;
        }
    }
}
