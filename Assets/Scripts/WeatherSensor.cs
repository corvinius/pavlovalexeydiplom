﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class WeatherSensor : UnitySensor
    {
        public WeatherSensor()
        {
            Sensor = new Sensor(new Weight(TypeWeight.Weather, 0));
        }
    }
}
