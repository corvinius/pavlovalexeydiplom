﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Train
{

    public float Speed;

    public float maxSpeed;

    public float SpeedOnPlot;

    public float koefSpeed;

    public float Friction;

    public bool isRain;


    public Train()
    {
        Friction = 1.5f;
    }
    public void Event(string Event)
    {
      switch(Event)
        {
                case "MiddleSpeed":
                {
                    if(SpeedOnPlot<60)
                    {
                        maxSpeed = SpeedOnPlot;
                        koefSpeed = 1;
                        break;
                    }
                    maxSpeed = 60;
                    koefSpeed = 1;
                    break;
                }
                case "Stop":
                {
                    maxSpeed = 0;
                    koefSpeed = 10;
                    break;
                }
               case "Normal":
                {
                    maxSpeed = SpeedOnPlot;
                    koefSpeed = 1;
                    break;
                }
        }
    }

    public void Update()
    {
        this.Speed = Mathf.Lerp(this.Speed, maxSpeed, Time.deltaTime/10*koefSpeed*Friction);
    }
}
