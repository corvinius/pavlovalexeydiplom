﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weight 
{
    public Weight(TypeWeight type,float value)
    {
        this.Type = type;
        this.Value = value;
    }

    public TypeWeight Type;
    public float Value;
}

public enum TypeWeight
{
    Visual,
    Traffic,
    Area,
    Weather,
}
