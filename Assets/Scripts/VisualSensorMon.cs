﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualSensorMon : MonoBehaviour
{
    public VisualSensor sensor;
    // Start is called before the first frame update
    void Awake()
    {
        sensor = new VisualSensor();
    }

    public void Update()
    {
        sensor.Update();
    }
  
}
