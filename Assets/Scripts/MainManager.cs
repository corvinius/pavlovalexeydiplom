﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainManager : MonoBehaviour
{

   // public List<UnitySensor> Unitysensors = new List<UnitySensor>();

    public Train train;

    public SensorController sensors;

    public Text Speed;
    public Text Friction;
    public InputField field;
    WeatherSensor weather;
    public Image TrainImage;

    public Area SelectedArea;
    public VisualSensorMon vis;

 
    public Image Rain;
    public Image Sun;

    public Sensor SelectedTraffic;

    public Image warning;


    public Image Green;
    public Image Yellow;
    public Image Red;
    public InputField SpeedOnArea;
    public RectTransform finish;

    [SerializeField]
    public Areas[] areas;

    public static bool NextBool(System.Random r,int truePercentage = 50)
    {
        return r.NextDouble() < truePercentage / 100.0;
    }

    public void Start()
    {
        train = new Train();
        weather = new WeatherSensor();
       // train.SpeedOnPlot = 120;


        System.Random r = new System.Random();
        bool rain =  NextBool(r);
        train.isRain = rain;
        CheckRain();

        sensors = new SensorController(train);
        SelectedTraffic = SelectedArea.light.Sensor;
        sensors.AddSensor(SelectedArea.light.Sensor);
        sensors.AddSensor(vis.sensor.Sensor);

        StartCoroutine(sensors.StartCheckSensors());

      
    }

    public void CheckArea()
    {
        Area sel = null;
        foreach (var item in areas)
        {
            if(TrainImage.rectTransform.position.x>item.b.position.x)
            {
                sel = item.area;
            }
        }
        if (sel != null)
        {
            train.SpeedOnPlot = sel.speed;
            sensors.RemoveSensor(SelectedTraffic);
            SelectedTraffic = sel.light.Sensor;
            sensors.AddSensor(SelectedTraffic);
        }
    }
    public void ChangeSpeed(string value)
    {
        float temp;
        if(float.TryParse(value,out temp))
        {
            train.SpeedOnPlot = temp;
        }
    }

    public void ChangeRain()
    {
        weather.Value = weather.Value== 0 ? 1 : 0;
        train.isRain = !train.isRain;
        CheckRain();
    }
    

    public void CheckRain()
    {
        if (train.isRain)
        {
            Rain.gameObject.SetActive(true);
            Sun.gameObject.SetActive(false);
            train.Friction = 2.5f;
        }
        else
        {
            Rain.gameObject.SetActive(false);
            Sun.gameObject.SetActive(true);
            train.Friction = 1.5f;
        }
    }


   

    public void CheckTraffic()
    {
        if(SelectedArea.light.Sensor.Weight.Value==0)
        {
            Green.gameObject.SetActive(true);
            Yellow.gameObject.SetActive(false);
            Red.gameObject.SetActive(false);
        }
        if (SelectedArea.light.Sensor.Weight.Value == 0.5f)
        {
            Green.gameObject.SetActive(false);
            Yellow.gameObject.SetActive(true);
            Red.gameObject.SetActive(false);
        }
        if (SelectedArea.light.Sensor.Weight.Value == 1)
        {
            Green.gameObject.SetActive(false);
            Yellow.gameObject.SetActive(false);
            Red.gameObject.SetActive(true);
        }
    }

    public void Update()
    {
        train.Update();
        TrainImage.rectTransform.position = Vector3.MoveTowards(TrainImage.rectTransform.position, finish.transform.position, train.Speed/10*Time.deltaTime);
        Speed.text =((int)train.Speed).ToString();
        Friction.text = (train.Friction).ToString();
        CheckTraffic();
        CheckArea();
    }

    public void ChangeTraffic(float value)
    {
        SelectedArea.light.tempValue = value;   
    }
    public void SetText(string speed)
    {
        SelectedArea.speed = float.Parse(speed);
    }
    public void SetArea(Area area)
    {
        SelectedArea = area;
        SpeedOnArea.text = SelectedArea.speed.ToString();
    }

    public void SetVisual()
    {
       

        if (vis.sensor.tempValue==0)
        {
            vis.sensor.tempValue = 1;
            warning.gameObject.SetActive(true);
        }
        else
        {
            vis.sensor.tempValue = 0;
            warning.gameObject.SetActive(false);

        }
    }
}

[System.Serializable]
public class Areas
{
    public RectTransform b;
    public Area area;
}
