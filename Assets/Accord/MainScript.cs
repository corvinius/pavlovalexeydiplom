﻿using Accord;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.Math;
using Accord.Math.Optimization.Losses;
using Accord.Statistics.Distributions.Multivariate;
using Accord.Statistics.Distributions.Sampling;
using Accord.Statistics.Filters;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using Vector3 = UnityEngine.Vector3; // Note: this might be necessary often

/// <summary>
///   Main controller for the sample application scene.
/// </summary>
/// 
/// <remarks>
///   This sample application simply uses a multivariate normal distribution (created
///   using the Accord.NET Framework) to move a sphere in a scene in random ways. This
///   is just an example to demonstrate how the Accord.NET libraries can be organized
///   and linked inside a Unity 3D project.
/// </remarks>
/// 
public class MainScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
       

    }

  
}
